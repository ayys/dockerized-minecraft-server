# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM alpine:latest as ServerDownloader
WORKDIR /
RUN apk add wget

RUN wget https://launcher.mojang.com/v1/objects/3dc3d84a581f14691199cf6831b71ed1296a9fdf/server.jar --output-document server.jar

# END OF ServerDownloader Stage



FROM alpine:latest

# Installing wget and openjdk8
RUN apk --no-cache add openjdk8

WORKDIR /minecraft

# Copy server.jar from ServerDownloader stage
COPY --from=ServerDownloader /server.jar .
# Copy eula.txt, server.properties to image
COPY minecraft-server/* ./

# start-server.sh is in minecraft-server dir
CMD ["sh", "./start-server.sh"]
