#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then		# check number of arguments
    echo $USAGE
    exit 1			# exit if server name not given
fi

USAGE="$0 <server name>"
SERVER_NAME=$1
source $MC_ROOTDIR/server-config.sh

create_directories() {
    mkdir $MC_SERVERDIR/$SERVER_NAME
}


copy_files() {
    cp $MC_ROOTDIR/minecraft-server/eula.txt\
       $MC_ROOTDIR/minecraft-server/server.properties\
       $MC_ROOTDIR/minecraft-server/Makefile\
       $MC_ROOTDIR/minecraft-server/docker-compose.yml\
	   $MC_ROOTDIR/minecraft-server/start-server.sh\
       $MC_SERVERDIR/$SERVER_NAME		# Destination
}

set_port() {
    final_port=$(tail -1 $MC_ROOTDIR/server_list | awk -F: '{print $2}')
    port=$((final_port + 1))
    sed -i "s/25565/$port/" $MC_ROOTDIR/servers/$SERVER_NAME/docker-compose.yml
    echo "$SERVER_NAME:$port" >> $MC_ROOTDIR/server_list
}


create_directories
copy_files
set_port
